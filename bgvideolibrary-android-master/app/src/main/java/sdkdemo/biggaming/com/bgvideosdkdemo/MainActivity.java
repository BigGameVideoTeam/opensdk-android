package sdkdemo.biggaming.com.bgvideosdkdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import org.cocos2dx.BGVideo.BGVideoActivity;
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onClick(View view){
        //URL从API获取
        String logMsg_ = "http://cdn.DEMO.com/pcH5/index.html?uid=105383813&token=DEMO06480785150F787DE82FCE1Febd9&sn=DEMO&account=T8194976&locale=zh_CN";
        //table参数参考对接文档
       logMsg_ = logMsg_ + "&table=b06";
        org.cocos2dx.BGVideo.BGVideoActivity.LoadBgVideo(this , logMsg_);
    }
    public void onClick2(View view){
        //URL从API获取
        String logMsg_ = "http://cdn.DEMO.com/pcH5/index.html?uid=105383813&token=DEMO06480785150F787DE82FCE1Febd9&sn=DEMO&account=T8194976&locale=zh_CN";
        //提示cui type 参数参考对接文档
        logMsg_ = logMsg_ + "&cui=512&type=1567";
        org.cocos2dx.BGVideo.BGVideoActivity.LoadBgVideo(this , logMsg_);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //   super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == org.cocos2dx.BGVideo.BGVideoActivity.BGSDK_EXIT_CODE) {
            Intent tIntent = this.getIntent();
            if (tIntent != null && data.getStringExtra("BGSDK").equals("SDKExit")) {
               // 退出SDK的回调
            }
        }
    }
}
