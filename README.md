# OpenSDK-android

_version update 2022-11-15_ (2.6.1)

OpenSDK-android 为BG真人娱乐Android平台SDK，
该SDK包含百家乐、极速百家乐、共咪百家乐、多彩百家乐、龙虎、轮盘、骰宝、牛牛、炸金花等游戏。

对应兼容线上版本:1.x.x.x 以上

### 文档修改日志

修改日期 | 对应线上版本号 | 修改内容
----|------|----
2020-04-13 | 0.18.6.2    | 1.廣告頁與自定義加載頁功能兼容與修正
2020-04-16 | 0.18.6.3    | 1.修正热更新断点续传 2.增加全資源版本aar(启动不必下载图像资源)
2020-07-24 | 0.19.0.0    | 1.引擎升级 2.逻辑优化调整 3.调整内部参数取用
2020-08-28 | 0.19.0.0    | 1.更新回调参数说明
2020-08-31 | 0.19.0.0    | 1.更新问题排除
2020-09-18 | 0.19.0.0    | 1.修正android P(api 28)以上，同时调用WebView崩溃问题
2020-09-21 | 0.19.0.4    | 1.优化选线逻辑 2.逻辑优化调整
2021-07-16 | 0.19.2.0    | 1.支援多台视频流程 
2022-01-27 | 0.19.2.20   | 1.添加Kotlin SDK导入
2022-03-17 | 0.19.2.23   | 1.资源更新 2.逻辑优化
2022-05-12 | 0.19.2.25   | 1.修正更新选线错误 2.优化资源包
2022-11-15 | 1.0.2.1     | 1.修正更新选线错误 2.优化资源包

## 对接方式

## 1. 引入OpenSDK

### java 导入： 
将示例项目中的 BGVideoLib.aar   BGVideoSDK.aar 文件根据使用架构复制对应版本到项目中的libs下,并在app的build.gradle的根节点下添加

    repositories{
        flatDir{

            dirs 'libs'

        }
    }    

在dependencies节点下添加添加

    compile(name: 'BGVideolib', ext: 'aar')

    compile(name: 'BGVideoSDK', ext: 'aar')
    
### Kotlin 导入：
将示例项目中的 BGVideoLib.aar   BGVideoSDK.aar 文件根据使用架构复制对应版本到项目中的libs下,并在app的build.gradle的dependencies节点下添加

    implementation(files("libs/BGVideolib.aar"))

    implementation(files("libs/BGVideoSDK.aar"))


## 2. 添加相应依赖

### java 导入： 
为避免添加依赖库的冲突，所以提出来一些常用的公共库，在外部app中添加依赖，需要在app的build.gradle中添加如下依赖

    compile 'com.android.support:multidex:1.0.2'

    implementation 'com.android.support:support-v4:26.0.0'

    implementation 'com.google.code.gson:gson:2.8.5'

添加非公用依赖库

    implementation files('libs/okhttp-3.12.7.jar')

    implementation files('libs/okio-1.15.0.jar')

    implementation files('libs/com.android.vending.expansion.zipfile.jar')

### Kotlin 导入：
为避免添加依赖库的冲突，所以提出来一些常用的公共库，在外部app中添加依赖，需要在app的build.gradle中添加如下依赖

    implementation 'com.android.support:multidex:1.0.2'

    implementation 'com.android.support:support-v4:26.0.0'

    implementation 'com.google.code.gson:gson:2.8.5'

添加非公用依赖库

    implementation(files('libs/okhttp-3.12.7.jar'))

    implementation(files('libs/okio-1.15.0.jar'))

    implementation(files('libs/com.android.vending.expansion.zipfile.jar'))

## 3. 调用方式

### java 
进入OpenSDK Activity

    import org.cocos2dx.BGVideo.BGVideoActivity;
 
    public void onClick(View view)
    {

        String logMsg_;
    
        org.cocos2dx.BGVideo.BGVideoActivity.LoadBgVideo(this , logMsg_);
    
    }

退出 OpenSDK Activity ，主Activity收到回调

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == org.cocos2dx.BGVideo.BGVideoActivity.BGSDK_EXIT_CODE) {
            Intent tIntent = this.getIntent();
            if (tIntent != null && data.getStringExtra("BGSDK").equals("SDKExit")) {
               // 退出OpenSDK SDK的回调
            }
        }
    }

### Kotlin 

    import org.cocos2dx.BGVideo.BGVideoActivity;

    fun Click1(view:View){
        var logMsg_ ;
        org.cocos2dx.BGVideo.BGVideoActivity.LoadBgVideo(this , logMsg_ );
    }

## 4. 问题排除

### Android 9 (API 28) 以上
AndroidManifest.xml 添加

    <uses-library android:name="org.apache.http.legacy" android:required="false"/>

### ABI支援
为追求包体轻量化，本SDK只包含armeabi-v7a ABI，若有ABI armeabi 或是 armeabi 混合 armeabi-V7a需求，可于本范例libs文件下找到对应ABI需求BGVideoSDK.aar做引入。

### buildTypes
避免使用minifyEnabled与shrinkResources参数，会导致系统崩溃。

### Android Q(API 29)
Android Q 权限调整，如要使用android.permission.READ_PHONE_STATE权限，SDK_VERSION不要使用29以上，会导致崩溃。
